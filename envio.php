<?php
$name = $_POST['name'];
$lastname = $_POST['lastname'];
$fullName = $name.' '.$lastname;
$email = $_POST['email'];
$telephone = $_POST['telephone'];
$msn = $_POST['msn'];
$result = '';

if (isset($name) && isset($lastname) && isset($email) && isset($telephone) && isset($msn) ) {
    $dest = "info@befor.com.co"; //Email de destino
    $asunto = "Escribió " . $fullName . " desde befor.com.co"; //Asunto
    $cuerpo = "
        <!DOCTYPE html>
        <html lang='es'>
        <head>
            <meta charset='UTF-8'>
        </head>
        <body>
            <header>
                <h1>Mensaje enviado desde befor.com.co</h1>
            </header>
            <div>
                <p>Usuario: " . $fullName . "</p>
                <p>Correo: " . $email . "</p>
                <p>Teléfono: " . $telephone . "</p>
                <p>Mensaje: " . $msn . "</p> 
            </div>
        </body>
        </html>"; //Cuerpo del mensaje
    //Cabeceras del correo
    $headers = "From: $fullName <$email>\r\n"; //Quien envia?
    $headers .= "X-Mailer: PHP5\n";
    $headers .= 'MIME-Version: 1.0' . "\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; //

    if (mail($dest, $asunto, $cuerpo, $headers)) {
        $result = '1';
    } else {
        $result = '0';
    }
}

echo $result;
