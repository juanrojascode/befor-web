<?php
include_once('./templates/header.html');
?>

<title>Servicios | Be for S.A.S.</title>
</head>

<body class="servicios loadershow">

    <?php
    include_once('./templates/menu.html');
    ?>

    <section class="center-vertical titulo bg-warning">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <p class="h1">
                        <span>Nuestros servicios</span>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-5 position-relative">
        <div class="list-group" id="list-tab" role="tablist">
            <a class="list-group-item list-group-item-action active" id="list-home-list" data-bs-toggle="list" href="#list-courses" role="tab" aria-controls="home">Nuestros cursos</a>
            <a class="list-group-item list-group-item-action" id="list-profile-list" data-bs-toggle="list" href="#list-consulting" role="tab" aria-controls="profile">Servicios de consultoría</a>
            <a class="list-group-item list-group-item-action" id="list-messages-list" data-bs-toggle="list" href="#list-production" role="tab" aria-controls="messages">Producción intelectual</a>
        </div>
    </div>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="list-courses" role="tabpanel" aria-labelledby="list-home-list">
            <div class="container-fluid">
                <div class="row bg-secundary">
                    <div class="col-12 col-md-6 center-vertical">
                        <div class="block-text text-light p-2 p-lg-5">
                            <p>Nuestro portafolio cuenta con los siguientes tipos de actividades de formación y capacitación:</p>
                            <div class="row justify-content-center my-4">
                                <div class="col-10">
                                    <table class="table table-sm bg-light">
                                        <thead>
                                            <tr>
                                                <th>Tipo de actividad</th>
                                                <th>Tiempo sugerido</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="table-warning">
                                                <td>Charla</td>
                                                <td>2 horas</td>
                                            </tr>
                                            <tr>
                                                <td>Taller (con alto componente práctico)</td>
                                                <td>De 3 a 8 horas</td>
                                            </tr>
                                            <tr class="table-warning">
                                                <td>Curso corto</td>
                                                <td>De 5 a 8 horas</td>
                                            </tr>
                                            <tr>
                                                <td>Seminario</td>
                                                <td>De 20 a 50 horas</td>
                                            </tr>
                                            <tr class="table-warning">
                                                <td>Diplomado</td>
                                                <td>Entre 70 y 120 horas</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <p>Para el caso de la formación abierta, mensualmente programamos las actividades en nuestro calendario de formación y capacitación.</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 p-0">
                        <div class="block-fluid-img">
                            <img src="./images/bg5.jpg" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 p-0 order-1 order-md-0">
                        <div class="block-fluid-img">
                            <img src="./images/bg6.jpg" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-md-6 center-vertical">
                        <div class="block-text p-5">
                            <p>Para la formación corporativa, diseñamos el programa a partir de la naturaleza y necesidades organizacionales y del público específico que hará parte de la actividad, por lo que dispondremos de un tiempo para trabajo conjunto con la organización aliada, pues de esta conversación no solo se determinan el tipo de actividad, tema y nombre, sino también el perfil del formador o tallerista. Todo esto nos permite establecer la información de costos y requerimientos para cada actividad planeada a la medida de sus necesidades.</p>
                            <p>Nuestros programas de educación se orientan a la formación abierta de profesionales y a la formación corporativa, en la medida en que el aprendizaje permanente y de toda la vida es fundamental para el crecimiento personal y organizacional.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mh-custom bg-warning center-vertical">
                <div class="container">
                    <div class="row justify-content-around align-items-center">
                        <div class="col-12 col-md-5">
                            <h5 class="mb-4 mt-5"><strong>Formación para el trabajo y desarrollo humano</strong></h5>
                            <p>Contamos, a modo de ejemplo, con las siguientes temáticas sobre las que ofrecemos nuestros programas para las dos modalidades (formación abierta profesional y formación corporativa):</p>
                        </div>
                        <div class="col-12 col-md-5">
                            <div class="accordion" id="accordionOne">
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <i class="fas fa-user-friends"></i> GESTIÓN DEL TALENTO HUMANO
                                        </button>
                                    </h2>
                                    <div id="collapseOne" class="accordion-collapse collapse" data-bs-parent="#accordionOne">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Gestión del conocimiento y aprendizaje organizacional.</li>
                                                <li>Nuevas tendencias en la gestión del talento humano.</li>
                                                <li>Cultura y clima organizacional.</li>
                                                <li>Planes de retiro.</li>
                                                <li>Teletrabajo y trabajo en casa.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <i class="fas fa-user-tie"></i> HABILIDADES GERENCIALES
                                        </button>
                                    </h2>
                                    <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#accordionOne">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Liderazgo.</li>
                                                <li>Negociación.</li>
                                                <li>Comunicación.</li>
                                                <li>Manejo de conflictos.</li>
                                                <li>Branding personal.</li>
                                                <li>Protocolo y etiqueta.</li>
                                                <li>Inteligencia emocional.</li>
                                                <li>Manejo del tiempo.</li>
                                                <li>Trabajo en equipo.</li>
                                                <li>Toma de decisiones.</li>
                                                <li>Reuniones efectivas.</li>
                                                <li>Mentoring y Coaching.</li>
                                                <li>Presentaciones efectivas.</li>
                                                <li>Pensamiento estratégico.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <i class="fas fa-sitemap"></i> ESTRATEGIA ORGANIZACIONAL
                                        </button>
                                    </h2>
                                    <div id="collapseThree" class="accordion-collapse collapse" data-bs-parent="#accordionOne">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Planes estratégicos.</li>
                                                <li>Emprendimiento y creación de empresa.</li>
                                                <li>Gerencia de la calidad.</li>
                                                <li>Gobernanza.</li>
                                                <li>Juntas directivas.</li>
                                                <li>Mercadeo estratégico .</li>
                                                <li>Finanzas estratégicas.</li>
                                                <li>Gerencia jurídica estratégica.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            <i class="fas fa-users"></i> RESPONSABILIDAD SOCIAL CORPORATIVA
                                        </button>
                                    </h2>
                                    <div id="collapseFour" class="accordion-collapse collapse" data-bs-parent="#accordionOne">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Responsabilidad social empresarial.</li>
                                                <li>Gestión de proyectos sociales.</li>
                                                <li>Ética y derechos humanos en las organizaciones.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            <i class="fas fa-venus"></i> MUJER Y GÉNERO
                                        </button>
                                    </h2>
                                    <div id="collapseFive" class="accordion-collapse collapse" data-bs-parent="#accordionOne">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Liderazgo femenino en las organizaciones.</li>
                                                <li>Competencias gerenciales de la mujer hoy.</li>
                                                <li>Marca personal y estilo femenino.</li>
                                                <li>Armonización trabajo – familia.</li>
                                                <li>Derechos de la mujer y feminismos.</li>
                                                <li>Prevención y manejo de situaciones de acoso laboral.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mh-custom center-vertical">
                <div class="container my-5">
                    <div class="row justify-content-around align-items-center">
                        <div class="col-12 col-md-5">
                            <h5 class="mb-4 mt-5"><strong>Formación para Instituciones de Educación Superior</strong></h5>
                            <p>Las Instituciones de Educación Superior- IES ha venido avanzando fuertemente en la última década y se han posicionado como un especial tipo de organización, que no solo se especifica por el desarrollo académico de su acción, sino por las mismas exigencias de dirección y gestión de ellas. Por eso, hemos considerado su especialidad para ofrecerles formación abierta profesional y formación corporativa en asuntos muy particulares de su labor.</p>
                            <p>Igualmente, estamos en capacidad de revisar necesidades particulares y crear o adaptar los escenarios y temas de formación.</p>
                            <p class="mb-4">Como temáticas especiales para IES contamos con:</p>
                        </div>
                        <div class="col-12 col-md-5">
                            <div class="accordion" id="accordionTwo">
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#aTwoCollapseOne" aria-expanded="true" aria-controls="aTwoCollapseOne">
                                            <i class="fas fa-tasks"></i> GESTIÓN CURRICULAR
                                        </button>
                                    </h2>
                                    <div id="aTwoCollapseOne" class="accordion-collapse collapse" data-bs-parent="#accordionTwo">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Creación y actualización de programas.</li>
                                                <li>Procesos de evaluación de programas.</li>
                                                <li>Internacionalización del currículo.</li>
                                                <li>Procesos de flexibilidad curricular.</li>
                                                <li>Competencias y resultados de aprendizaje.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#aTwocollapseTwo" aria-expanded="false" aria-controls="aTwocollapseTwo">
                                            <i class="fas fa-chalkboard-teacher"></i> GESTIÓN DOCENTE
                                        </button>
                                    </h2>
                                    <div id="aTwocollapseTwo" class="accordion-collapse collapse" data-bs-parent="#accordionTwo">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Nuevas metodologías y estrategias didácticas.</li>
                                                <li>Evaluación del aprendizaje.</li>
                                                <li>Currículo para docentes.</li>
                                                <li>Investigación en el aula.</li>
                                                <li>Proyectos sociales en el aula.</li>
                                                <li>Identidad institucional.</li>
                                                <li>Normatividad institucional y de programas para docentes.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#aTwoCollapseThree" aria-expanded="false" aria-controls="aTwoCollapseThree">
                                            <i class="fas fa-search"></i> GESTIÓN DE LA INVESTIGACIÓN
                                        </button>
                                    </h2>
                                    <div id="aTwoCollapseThree" class="accordion-collapse collapse" data-bs-parent="#accordionTwo">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Introducción a la investigación en la universidad.</li>
                                                <li>Temáticas específicas para la investigación según necesidades disciplinares y metodológicas de proyectos.</li>
                                                <li>Gestión y liderazgo de grupos de investigación.</li>
                                                <li>Escritura para la investigación.</li>
                                                <li>Investigación formativa y formación para la investigación.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#aTwocollapseThree" aria-expanded="false" aria-controls="aTwocollapseThree">
                                            <i class="fas fa-shield-alt"></i> ASEGURAMIENTO DE LA CALIDAD
                                        </button>
                                    </h2>
                                    <div id="aTwocollapseThree" class="accordion-collapse collapse" data-bs-parent="#accordionTwo">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Lineamientos y normativas de aseguramiento de la calidad nacionales e internacionales.</li>
                                                <li>Construcción de equipos para el aseguramiento de la calidad.</li>
                                                <li>Comunicación estratégica para el aseguramiento de la calidad.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#aTwoCollapseFive" aria-expanded="false" aria-controls="aTwoCollapseFive">
                                            <i class="fas fa-users"></i> GESTIÓN DE LA PROYECCIÓN SOCIAL Y LA EXTENSIÓN UNIVERSITARIA
                                        </button>
                                    </h2>
                                    <div id="aTwoCollapseFive" class="accordion-collapse collapse" data-bs-parent="#accordionTwo">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Construcción de proyectos sociales.</li>
                                                <li>Fundraising para proyectos sociales.</li>
                                                <li>Gestión estratégica de egresados y graduados.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#aTwoCollapseSix" aria-expanded="false" aria-controls="aTwoCollapseSix">
                                            <i class="fas fa-map-signs"></i> DIRECCIONAMIENTO ESTRATÉGICO DE IES
                                        </button>
                                    </h2>
                                    <div id="aTwoCollapseSix" class="accordion-collapse collapse" data-bs-parent="#accordionTwo">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Planes estratégicos para la Educación Superior.</li>
                                                <li>Gobernanza en la Educación Superior.</li>
                                                <li>Finanzas estratégicas para la Educación Superior.</li>
                                                <li>Construcción de identidad institucional y académica.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#aTwoCollapseSeven" aria-expanded="false" aria-controls="aTwoCollapseSeven">
                                            <i class="fas fa-university"></i> BIENESTAR UNIVERSITARIO Y FORMACIÓN INTEGRAL
                                        </button>
                                    </h2>
                                    <div id="aTwoCollapseSeven" class="accordion-collapse collapse" data-bs-parent="#accordionTwo">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Cursos dirigidos a estudiantes, profesores, funcionarios administrativos y población externa en temas de cultura, arte, deportes, bienestar personal, etc.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="list-consulting" role="tabpanel" aria-labelledby="list-profile-list">
            <div class="mh-custom bg-warning center-vertical">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6 center-vertical">
                            <div class="block-text p-5">
                                <p>Nuestra área de consultoría se orienta en dos direcciones: una para instituciones de educación superior, y otra para todo tipo de organización pública o privada.</p>
                                <h5 class="my-4"><strong>Consultoría corporativa en procesos de formación</strong></h5>
                                <p>Nuestra consultoría corporativa se centra en los procesos de formación del talento humano de la organización, que abarca desde la vinculación del colaborador, hasta su eventual retiro.</p>
                                <p>En este sentido, ofrecemos los siguientes servicios:</p>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 d-flex justify-content-center flex-column">
                            <div class="accordion" id="accordionThree">
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThreeOne" aria-expanded="true" aria-controls="collapseThreeOne">
                                            <i class="fas fa-user-friends"></i> PLANES DE FORMACIÓN ORGANIZACIONAL
                                        </button>
                                    </h2>
                                    <div id="collapseThreeOne" class="accordion-collapse collapse" data-bs-parent="#accordionThree">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Diseño del plan según necesidades de la organización.</li>
                                                <li>Diseño de los cursos en las modalidades más apropiadas para la organización.</li>
                                                <li>Desarrollo de los cursos con nuestros formadores, capacitadores y talleristas.</li>
                                                <li>Valoración de resultados.</li>
                                                <li>Articulación del plan de formación con los procesos de inducción, ascenso y retiro.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThreeTwo" aria-expanded="false" aria-controls="collapseThreeTwo">
                                            <i class="fas fa-user-tie"></i> CURSOS DE FORMACIÓN PARA NECESIDADES ESPECIALES
                                        </button>
                                    </h2>
                                    <div id="collapseThreeTwo" class="accordion-collapse collapse" data-bs-parent="#accordionThree">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Para procesos de cambio organizacional.</li>
                                                <li>Para procesos de coyuntura.</li>
                                                <li>Por situaciones de crisis.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThreeThree" aria-expanded="false" aria-controls="collapseThreeThree">
                                            <i class="fas fa-sitemap"></i> PLANES DE MENTORING Y COACHING
                                        </button>
                                    </h2>
                                    <div id="collapseThreeThree" class="accordion-collapse collapse" data-bs-parent="#accordionThree">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Para la incorporación.</li>
                                                <li>Para el desarrollo de directivos.</li>
                                                <li>Para apoyo a directivos.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mh-custom center-vertical">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="block-text p-5">
                                <h5 class="my-4"><strong>Consultoría para IES</strong></h5>
                                <p>Dada la experiencia en IES, estamos en capacidad de ofrecer a estas organizaciones consultorías completas e integrales en todas sus áreas específicas, así:</p>
                            </div>
                            <div class="accordion" id="accordionFour">
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="fas fa-file"></i> DOCUMENTOS INSTITUCIONALES FUNDANTES
                                        </button>
                                    </h2>
                                    <div id="collapseOne" class="accordion-collapse collapse" data-bs-parent="#accordionFour">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Estatutos de la institución.</li>
                                                <li>Proyecto Educativo Institucional – PEI.</li>
                                                <li>Planes estratégicos o de desarrollo.</li>
                                                <li>Lineamientos generales (curriculares, de docencia, de investigación, de proyección social, bienestar universitario, aseguramiento de la calidad, etc.).</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <i class="fas fa-user-tie"></i> NORMATIVAS
                                        </button>
                                    </h2>
                                    <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#accordionFour">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Estatuto y escalafón profesoral.</li>
                                                <li>Reglamentos estudiantiles.</li>
                                                <li>Reglamentos de investigación, ciencia, tecnología e innovación.</li>
                                                <li>Reglamentos de procesos específicos (prácticas, de procesos y opciones de grado, formación para la investigación, elección de representantes ante órganos colegiados, etc.).</li>
                                                <li>Estatuto y reglamentos de personal administrativo.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <i class="fas fa-sitemap"></i> GESTIÓN DE ASEGURAMIENTO DE LA CALIDAD
                                        </button>
                                    </h2>
                                    <div id="collapseThree" class="accordion-collapse collapse" data-bs-parent="#accordionFour">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Lineamientos y políticas generales.</li>
                                                <li>Sistema de Aseguramiento de la Calidad.</li>
                                                <li>Diseño de los procesos y procedimientos relacionados con la gestión del aseguramiento de la calidad.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            <i class="fas fa-sitemap"></i> GESTIÓN CURRICULAR
                                        </button>
                                    </h2>
                                    <div id="collapseFour" class="accordion-collapse collapse" data-bs-parent="#accordionFour">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Lineamientos y políticas generales en materia curricular y pedagógica.</li>
                                                <li>Procesos de desarrollo y gestión curricular.</li>
                                                <li>Estudios de mercado y estudios de factibilidad.</li>
                                                <li>Procesos de creación de programas.</li>
                                                <li>Procesos de actualización de programas.</li>
                                                <li>Procesos de flexibilización curricular.</li>
                                                <li>Procesos de internacionalización del currículo.</li>
                                                <li>Diseño de los procesos y procedimientos relacionados con la gestión curricular.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            <i class="fas fa-sitemap"></i> GESTIÓN PROFESORAL
                                        </button>
                                    </h2>
                                    <div id="collapseFive" class="accordion-collapse collapse" data-bs-parent="#accordionFour">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Procesos de vinculación de profesores.</li>
                                                <li>Procesos de evaluación de profesores.</li>
                                                <li>Planes de formación profesorales.</li>
                                                <li>Diseño de los procesos y procedimientos relacionados con la gestión de la profesoral.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 d-flex justify-content-center flex-column">
                            <div class="accordion" id="accordionFive">
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordinFiveOne" aria-expanded="true" aria-controls="accordinFiveOne">
                                            <i class="fas fa-user-friends"></i> GESTIÓN DE ESTUDIANTES
                                        </button>
                                    </h2>
                                    <div id="accordinFiveOne" class="accordion-collapse collapse" data-bs-parent="#accordionFive">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Proyectos y planes de admisiones de estudiantes.</li>
                                                <li>Programas de becas y ayudas económicas.</li>
                                                <li>Procesos de éxito académico y seguimiento.</li>
                                                <li>Diseño de los procesos y procedimientos relacionados con la gestión de estudiantes.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordinFiveTwo" aria-expanded="false" aria-controls="accordinFiveTwo">
                                            <i class="fas fa-user-tie"></i> GESTIÓN DE LA INVESTIGACIÓN
                                        </button>
                                    </h2>
                                    <div id="accordinFiveTwo" class="accordion-collapse collapse" data-bs-parent="#accordionFive">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Lineamientos y políticas generales.</li>
                                                <li>Planes estratégicos de investigación.</li>
                                                <li>Procesos de creación y desarrollo de grupos de investigación.</li>
                                                <li>Convocatorias de investigación.</li>
                                                <li>Procesos </li>
                                                <li>Diseño de los procesos y procedimientos relacionados con la gestión de la investigación.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accirdubFiveThree" aria-expanded="false" aria-controls="accirdubFiveThree">
                                            <i class="fas fa-sitemap"></i> GESTIÓN DE LA PROYECCIÓN SOCIAL Y EXTENSIÓN
                                        </button>
                                    </h2>
                                    <div id="accirdubFiveThree" class="accordion-collapse collapse" data-bs-parent="#accordionFive">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Lineamientos y políticas generales.</li>
                                                <li>Planes estratégicos de proyección social y extensión.</li>
                                                <li>Planes de seguimiento a graduados.</li>
                                                <li>Proyectos <i>longlife learning</i>.</li>
                                                <li>Diseño de los procesos y procedimientos relacionados con la gestión de la de la proyección social y extensión.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordinFiveFour" aria-expanded="false" aria-controls="accordinFiveFour">
                                            <i class="fas fa-sitemap"></i> GESTIÓN DEL BIENESTAR UNIVERSITARIO
                                        </button>
                                    </h2>
                                    <div id="accordinFiveFour" class="accordion-collapse collapse" data-bs-parent="#accordionFive">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Lineamientos y políticas generales.</li>
                                                <li>Planes de bienestar según tipo de poblaciones. </li>
                                                <li>Diseño de los procesos y procedimientos relacionados con la gestión del bienestar universitario.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordinFiveFive" aria-expanded="false" aria-controls="accordinFiveFive">
                                            <i class="fas fa-sitemap"></i> GESTIÓN JURÍDICA DE LA IES
                                        </button>
                                    </h2>
                                    <div id="accordinFiveFive" class="accordion-collapse collapse" data-bs-parent="#accordionFive">
                                        <div class="accordion-body">
                                            <ul>
                                                <li>Estrategia jurídica.</li>
                                                <li>Procedimientos de seguimiento a procesos judiciales.</li>
                                                <li>Manejo jurídico de sindicatos y asociaciones.</li>
                                                <li>Asesoría jurídica para la gestión de la educación ante entidades.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="list-production" role="tabpanel" aria-labelledby="list-messages-list">
            <div class="container-fluid center-vertical">
                <div class="row bg-warning">
                    <div class="col-12 col-md-6 center-vertical">
                        <div class="block-text text-dark p-5">
                            <p>Para todas nuestras organizaciones aliadas, también contribuimos en la elaboración de los documentos que requiera la organización, a partir de la naturaleza, identidad y necesidades de cada una, o haciendo una revisión “de pares” para retroalimentar la documentación ya realizada previamente con recomendaciones de ajustes o modificación.</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 p-0">
                        <div class="block-fluid-img">
                            <img src="./images/bg7.jpg" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    include_once('./templates/footer.html');
    ?>