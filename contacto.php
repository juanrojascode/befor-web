<?php
include_once('./templates/header.html');
?>

<title>Contacto | Be for S.A.S.</title>
</head>

<body class="loadershow">

    <?php
    include_once('./templates/menu.html');
    ?>

    <section class="bg-secundary center-vertical titulo">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <p class="h1 text-light">
                        Ponte en contacto con nosotros
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="rmh center-vertical py-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6 center-vertical">
                    <div class="block-text text-center px-md-4">
                        <h1>Contacto</h1>
                        <p><i class="fas fa-envelope"></i> info@befor.com.co</p>
                        <p><i class="fas fa-mobile-alt"></i> (+57) 305 235 0263</p>
                    </div>
                </div>
                <div class="col-12 col-md-6 center-vertical">
                    <div class="block-text text-center px-md-4">
                        <h1>Ubicación</h1>
                        <p><i class="fas fa-map-marker-alt me-1"></i>Kilómetro 4.7 Vía Chía-Cajicá,<br>Centro Empresarial Sabana Park<br>Torre 3, Oficina 506<br>Cajicá, Cundinamarca, Colombia</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-warning center-vertical">
        <div class="container-fluid">
            <div class="row mh-custom">
                <div class="col-12 col-lg-6 p-0">
                    <div class="block-fluid-img h-100">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9454.71662702162!2d-74.03359389463579!3d4.903034794782308!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e40782a020a3779%3A0x3e8e655e2f1ddbc2!2sSabana%20Park%20Health%20%26%20Business!5e0!3m2!1ses-419!2sco!4v1617314003429!5m2!1ses-419!2sco" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
                <div class="col-12 col-lg-6 center-vertical">
                    <div class="block-text p-5">
                        <div class="formContainer">
                            <form id="formMessage" method="post" class="form-horizontal row g-3" action="">
                                <div class="col-12 col-md-6">
                                    <label for="name" class="form-label">Nombre</label>
                                    <input type="text" class="form-control" name="name" id="name">
                                </div>
                                <div class="col-12 col-md-6">
                                    <label for="lastname" class="form-label">Apellido</label>
                                    <input type="text" class="form-control" name="lastname" id="lastname">
                                </div>
                                <div class="col-12 col-md-6">
                                    <label for="email" class="form-label">Correo electrónico</label>
                                    <input type="email" class="form-control" name="email" id="email">
                                </div>
                                <div class="col-12 col-md-6">
                                    <label for="telephone" class="form-label">Teléfono</label>
                                    <input type="tel" maxlength="10" min="1" class="form-control" name="telephone" id="telephone">
                                </div>
                                <div class="col-12">
                                    <label for="messagecontent" class="form-label">Mensaje</label>
                                    <textarea type="text" class="form-control" name="messagecontent" id="messagecontent"></textarea>
                                </div>
                                <div class="col-12 text-center">
                                    <button class="btn btn-dark g-recaptcha" data-sitekey="6LcHpp8aAAAAANoMRxZft9CqfgWuChEcXNLp6yRD" data-callback='onSubmit' data-action='submit' id="envio">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
    include_once('./templates/footer.html');
    ?>