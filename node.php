<?php
include_once('./templates/header.html');
?>

<title>¿Ahorrar o no ahorrar? | Be for S.A.S.</title>
</head>

<body class="node loadershow">

    <?php
    include_once('./templates/menu.html');
    ?>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10">
                <div class="node-title my-5">
                    <h2>¿Ahorrar o no ahorrar?</h2>
                    <div class="node-date">
                        <span>Publicado el viernes 14 de mayo del 2021</span>
                    </div>
                </div>
                <div class="node-container">
                    <p>Muchos de nosotros, hemos conocido de organizaciones tanto públicas como privadas, que en esta época de crisis global que estamos viviendo, han debido hacer recortes y ajustes presupuestales con el fin de entrar en planes de austeridad y solo así, “gastar” en lo estrictamente necesario para velar no solo por la vida de la organización, sino para no afectar la vinculación de sus colaboradores, bien como trabajadores directos o como aportantes indirectos. Esto pues, al margen de críticas que algunos pueden hacer cuando no se está de acuerdo con el manejo de las finanzas corporativas en medio de la crisis.</p>

                    <p>Aún así, en muchas ocasiones uno de los aspectos que se han tocado para ajustar los presupuestos, son los procesos de formación y capacitación de las organizaciones, lo que deja sinsabores no solo en los trabajadores, sino en los equipos de la organización a cargo de esta labor y que son quienes les ponen el alma a estos procesos.</p>

                    <p>Sin embargo, es importante que cuando se vayan a hacer estos análisis de ahorro o austeridad, y antes de empezar a recortar iniciando con los presupuestos de formación y capacitación, se considere lo que la organización también “pierde” cuando suspende estas acciones, ya que los beneficios que esto conlleva son los que se van a afectar directamente; beneficios que se reportan no solo para el colaborador sino para la organización en si misma, y un poco más a largo plazo para la sociedad en general en la que estamos insertos.</p>

                    <p>Es muy claro para la mayoría, que la formación y capacitación organizacional brindada a sus colaboradores, beneficia de modo directo al trabajador individualmente considerado, por ejemplo, aumentando su conocimiento de la organización, incrementando la satisfacción personal con su trabajo, mejorando las competencias profesionales y personales, aportando al desarrollo también tanto profesional y personal, promoviendo mejores relaciones interpersonales al interior de la organización e identificando sus posibilidades de ascenso o promoción, robusteciendo su hoja de vida, entre muchas otras, seguramente.</p>

                    <p>Estos beneficios personales, al mismo tiempo, son beneficios organizacionales, pues con ello mismo, la organización podrá mejorar el nivel de eficiencia y eficacia de sus trabajadores y por ende de la organización como un todo; facilitará la innovación y el cambio no solo desde el escenario de la proposición, sino de la aceptación de ello; podrá mejorar su clima organizacional y trabajar por una cultura organizacional sana y con identidad sólida; disminuirá las tasas de rotación del personal; podrá construir con mayor facilidad los planes de ascenso y promoción y ser eficaz en ello, etc. Todo esto, es lo que al final de cuentas contribuirá a la construcción de su <em>employer branding</em> o reputación del empleador, lo que permitirá también atraer a excelentes talentos, pero que al final de cuentas, terminará contribuyendo al crecimiento e impacto de <strong>la organización como conjunto</strong> con todos sus <em>stakeholders</em> tanto directos como indirectos.</p>

                    <p>Así, pues, antes de recortar en estos planes, hay que pensar cómo se está impactando este último asunto al que se llega: crecimiento e impacto de la organización como conjunto, y entonces preguntarse ¿vale la pena este ahorro?</p>

                </div>
                <div class="node-author mb-5">
                    <p>Escrito por: <span><strong><em>Claudia Helena Forero</em></strong></span></p>
                </div>
            </div>
        </div>
    </div>

    <?php
    include_once('./templates/footer.html');
    ?>