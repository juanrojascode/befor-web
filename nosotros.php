<?php
include_once('./templates/header.html');
?>

<title>Acerca de nosotros | Be for S.A.S.</title>
</head>

<body class="loadershow">

    <?php
    include_once('./templates/menu.html');
    ?>

    <section class="bg-warning center-vertical titulo">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <p class="h1">
                        Vamos a conocernos mejor
                    </p>
                </div>
            </div>
        </div>
    </section>



    <section class="bg-secundary center-vertical">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6 p-0 order-1 order-md-0">
                    <div class="block-fluid-img">
                        <img src="./images/bg2.jpg" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="col-12 col-md-6 center-vertical">
                    <div class="block-text text-light p-5">
                        <p class="h2">Los educadores son innovadores</p>
                        <p>No se puede llegar muy lejos en nuestra industria sin una buena idea de cómo piensa la gente y qué limita su potencial. Es esta comprensión la que nos coloca en la posición perfecta para ayudar a nuestros clientes a descargarse de comportamientos ineficaces.</p>
                        <p>No nos enorgullecemos de nuestro trabajo, sino de los resultados de nuestro trabajo.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-warning center-vertical py-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6">
                    <figure>
                        <img src="/images/socias-2-2.jpg" alt="" class="img-fluid">
                    </figure>
                </div>
                <div class="col-12 col-md-6 center-vertical">
                    <div class="block-text px-4">
                        <p class="h2 text-secondary-p">Claudia Helena</p>
                        <p class="h5 mb-2">Forero Forero</p>
                        <p>Abogada con experiencia en dirección en el sector académico de la Educación Superior, en las funciones de docencia, investigación y en especial en gestión académica en materia curricular, de autoevaluación de programas y de procesos de acreditación.</p>
                        <p>Gran interés por las actividades de gestión, planeación, evaluación y dirección de procesos académicos en Educación Superior.</p>
                        <p><strong>Especialidades:</strong> planeación, gestión, evaluación y dirección de procesos académicos en Educación Superior.</p>
                        <p><strong>Últimos cargos:</strong> Vicerrectora Académica y Decana de Facultad de Derecho de la Universidad Militar, Directora de Procesos Académicos del Instituto de Postgrados Forum de la Universidad de La Sabana.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="center-vertical py-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6 center-vertical">
                    <div class="block-text px-4">
                        <p class="h2 text-warning">Anneth Eliana</p>
                        <p class="h5 mb-2">Beltrán Mayorga</p>
                        <p>Comunicadora Social y Periodista  con experiencia en gestión en el sector académico de la Educación Superior y Tecnología, en las funciones de docencia y extensión.</p>
                        <p>Con interés por las actividades de mercadeo, difusión y comunicación en la educación para el trabajo.</p>
                        <p><strong>Especialidades:</strong> Comunicación, gestión, mercadeo y extensión de procesos académicos en Educación Superior y Tecnología.</p>
                        <p><strong>Últimos cargos:</strong> Docente de la Universidad Militar Nueva Granada, Escuela Superior de Guerra y Escuela de Misiones Internacionales y Acción Integral. Jefe de Prensa en  el Congreso de la República, Coordinadora Beca Excelencia Universidad de La Sabana.</p>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <figure>
                        <img src="/images/socias-3-3.jpg" alt="" class="img-fluid">
                    </figure>
                </div>                
            </div>
        </div>
    </section>

    <?php
    include_once('./templates/footer.html');
    ?>