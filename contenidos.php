<?php
include_once('./templates/header.html');
?>

<title>Servicios | Be for S.A.S.</title>
</head>

<body class="servicios loadershow">

    <?php
    include_once('./templates/menu.html');
    ?>

    <section class="center-vertical titulo bg-warning">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <p class="h1">
                        <span>Ideas para ti</span>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-5 position-relative">
        <div class="list-group" id="list-tab" role="tablist">
            <a class="list-group-item list-group-item-action active" id="list-thiks-list" data-bs-toggle="list" href="#list-thiks" role="tab" aria-controls="thiks">Thinks</a>
            <a class="list-group-item list-group-item-action" id="list-tips-list" data-bs-toggle="list" href="#list-tips" role="tab" aria-controls="tips">Tips</a>
            <!-- <a class="list-group-item list-group-item-action" id="list-messages-list" data-bs-toggle="list" href="#list-production" role="tab" aria-controls="messages">Producción intelectual</a> -->
        </div>
    </div>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active columnas" id="list-thiks" role="tabpanel" aria-labelledby="list-thiks-list">
            <div class="container mb-5">
                <div class="row justify-content-center">
                    <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                        <div class="card">
                            <img src="https://i.picsum.photos/id/202/200/200.jpg?hmac=eGzhW5P2k0gzjc76Tk5T9lOfvn30h3YHuw5jGnBUY4Y" class="card-img-top" alt="imagen de prueba">
                            <div class="card-body">
                                <h5 class="card-title columnas-title fw-bold">¿Ahorrar o no ahorrar?</h5>
                                <p class="card-text">Muchos de nosotros, hemos conocido de organizaciones tanto públicas como privadas, que en esta época de crisis global que estamos viviendo, han debido hacer recortes y ajustes presupuestales con el fin...</p>
                                <a href="node" class="link-dark">Leer más <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                        <div class="card">
                            <img src="https://i.picsum.photos/id/326/200/200.jpg?hmac=T_9V3kc7xrK46bj8WndwDhPuvpbjnAM3wfL_I7Gu6yA" class="card-img-top" alt="imagen de prueba">
                            <div class="card-body">
                                <h5 class="card-title columnas-title fw-bold">Nombre de la columna</h5>
                                <p class="card-text">Descripción Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet, earum.</p>
                                <a href="#" class="link-dark">Leer más <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                        <div class="card">
                            <img src="https://i.picsum.photos/id/599/200/200.jpg?hmac=2WLKs3sxIsaEQ-6WZaa6YMxgl6ZC4cNnid0aqupm2is" class="card-img-top" alt="imagen de prueba">
                            <div class="card-body">
                                <h5 class="card-title columnas-title fw-bold">Nombre de la columna</h5>
                                <p class="card-text">Descripción Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet, earum.</p>
                                <a href="#" class="link-dark">Leer más <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                        <div class="card">
                            <img src="https://i.picsum.photos/id/515/200/200.jpg?hmac=d6WMJkHOOB7pT-6y_yjHKrJdVp3v17ry6bMzGVuyb68" class="card-img-top" alt="imagen de prueba">
                            <div class="card-body">
                                <h5 class="card-title columnas-title fw-bold">Nombre de la columna</h5>
                                <p class="card-text">Descripción Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet, earum.</p>
                                <a href="#" class="link-dark">Leer más <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="list-tips" role="tabpanel" aria-labelledby="list-tips-list">
            <div class="instagram py-5">
                <div class="container">
                    <div class="info"></div>
                    <p class="h2 text-center">Instagram</p>
                    <div data-mc-src="a4e4791c-9afc-483c-a374-c65dca6b441f#instagram"></div>
                </div>
            </div>
        </div>
        <!-- <div class="tab-pane fade" id="list-production" role="tabpanel" aria-labelledby="list-messages-list">
            <div class="container-fluid center-vertical">
                <div class="row bg-warning">
                    <div class="col-12 col-md-6 center-vertical">
                        <div class="block-text text-dark p-5">
                            <p>Para todas nuestras organizaciones aliadas, también contribuimos en la elaboración de los documentos que requiera la organización, a partir de la naturaleza, identidad y necesidades de cada una, o haciendo una revisión “de pares” para retroalimentar la documentación ya realizada previamente con recomendaciones de ajustes o modificación.</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 p-0">
                        <div class="block-fluid-img">
                            <img src="./images/bg7.jpg" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>

    <?php
    include_once('./templates/footer.html');
    ?>