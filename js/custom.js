(function () {
  header();
  menu();
  sendMessage();
  wtsBtn();
  heightFeedIg();
  setTimeout(() => {
    loader(false);
  }, 500);
})();

/**
 * Menu animación cuando pasa a fixed
 */
function header() {
  $(window).scroll(() => {
    let windowTop = $(window).scrollTop();
    windowTop > 96
      ? $(".menuContainer").addClass(
          "navShadow animate__animated animate__fadeInDown"
        )
      : $(".menuContainer").removeClass(
          "navShadow animate__animated animate__fadeInDown"
        );
  });
}

/**
 * Animación de X en menú responsive
 * Coloca el 'active' en el elemento según página actual
 */
function menu() {
  $(".menu").click(function (e) {
    e.preventDefault();
    $(this).toggleClass("show");
  });

  let pathname = window.location.pathname;
  $("#navbarPrincipal a").each(function () {
    var href = $(this).attr("href");
    var indice = pathname.length - href.length;
    if (pathname.substring(indice) === href) {
      $(this).addClass("active");
    }
  });
}

/**
 * Realiza el envío del formulario con los datos suministrados
 */
function sendMessage() {
  $("#envio").click(function (e) {
    e.preventDefault();
    if (validForm()) {
      let name = $("#name").val(),
        lastname = $("#lastname").val(),
        email = $("#email").val(),
        telephone = $("#telephone").val(),
        messagecontent = $("#messagecontent").val();
      $.ajax({
        type: "POST",
        url: "envio",
        data: {
          name: name,
          lastname: lastname,
          email: email,
          telephone: telephone,
          msn: messagecontent,
        },
        dataType: "text",
        beforeSend: function () {
          loader(true);
        },
        success: function (response) {
          switch (response) {
            case "0":
              loader(false);
              showSweetAlert(
                "error",
                "Lo sentimos",
                "Verifica que todos los campos estén completos.",
                "Entendido"
              );
              break;

            case "1":
              loader(false);
              showSweetAlert(
                "success",
                "¡Muy bien!",
                "Te contactaremos lo más pronto posible",
                "Continuar"
              );
              break;

            default:
              loader(false);
              showSweetAlert(
                "error",
                "Lo sentimos",
                "Ponte en contacto con el administrador",
                "Vale"
              );
              break;
          }
        },
      });
    }
  });
}

/**
 * Valida el formulario y retorna true o false
 * @returns Bool
 */
function validForm() {
  let form = $("#formMessage");

  form.validate({
    rules: {
      name: {
        required: true,
        minlength: 3,
      },
      lastname: {
        required: true,
        minlength: 3,
      },
      telephone: {
        required: true,
        minlength: 10,
      },
      email: {
        required: true,
        email: true,
      },
      messagecontent: {
        required: true,
        minlength: 20,
      },
    },
    messages: {
      name: {
        required: "Ingrese su nombre",
        minlength: "El celular debe contener 3 números mínimo.",
      },
      lastname: {
        required: "Ingrese sus apellidos",
        minlength: "El celular debe contener 3 números mínimo.",
      },
      telephone: {
        required: "Por favor ingrese un número de celular",
        minlength: "El celular debe contener 10 números mínimo.",
      },
      email: {
        required: "Escriba un correo electrónico",
        email: "Escriba un correo electrónico válido",
      },
      messagecontent: {
        required: "Escribe cómo te podemos ayudar",
        minlength: "El mensaje debe tener al menos 20 caracteres.",
      },
    },
    errorElement: "span",
    errorClass: "is-invalid",
    validClass: "is-valid",
  });

  return form.valid();
}

/**
 * Habilita o no el loader
 * @param {Bool} val true or false
 */
function loader(val) {
  let item = $(".loader"),
    bd = $("body");
  if (val) {
    item.fadeIn();
    bd.addClass("loadershow");
  } else {
    item.fadeOut();
    bd.removeClass("loadershow");
  }
}

/**
 * Muestra botón de Whatsapp
 */
function wtsBtn() {
  let link =
    '<a href="http://api.whatsapp.com/send?phone=573052350263&text=Hola, estoy interesado en sus servicios." target="_blank" title="Whatsapp"><img class="img-fluid" src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/whatsapp.svg" /></a>';
  $("<div>", {
    id: "WAButton",
  }).appendTo("footer");
  $("#WAButton").append(link);
  /* console.log('this'); */
}

/**
 * Swal popup message
 * @param {string} icon
 * @param {string} title
 * @param {string} message
 * @param {string} textConfirmButton
 */
function showSweetAlert(icon, title, message, textConfirmButton) {
  Swal.fire({
    icon: icon,
    title: title,
    text: message,
    confirmButtonText: textConfirmButton,
  });
}

/**
 * Función para ocultar el "free" del widget Feed IG
 */
function heightFeedIg() {
  let empty = 0;
  $(document).scroll(function () {
    let elem = $(".instagram article > div"),
      eValue = elem.height();

    //Validamos si el valor guardado es el mismo al obtenido,
    //de no ser así, resta 20 y ajusta el alto
    if (eValue != empty) {
      let v = eValue - 20;
      elem.height(v);
      empty = v;
    }
  });
}
