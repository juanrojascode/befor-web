<?php
include_once 'admin/core/conexion.php';

//LEER TABLA
//$sql_leer = 'SELECT * FROM sliders ORDER BY date_creada DESC';
$sql_leer = 'SELECT * FROM sliders ORDER BY date_create DESC';
$gsent = $pdo->prepare($sql_leer);
$gsent->execute();
$resultado = $gsent->fetchAll();

/* echo '<pre>';
var_dump($resultado);
echo '</pre>'; */

include_once('./templates/header.html');

?>

<title>Be for S.A.S.</title>
</head>

<body class="home loadershow">

  <?php
  include_once('./templates/menu.html');
  ?>

  <section class="sliderAnuncio rmh">
    <div id="sliderDeskop" class="carousel slide d-none d-lg-block" data-bs-ride="carousel">
      <div class="carousel-inner">
        <?php
        $init = 0;
        foreach ($resultado as $dato) :
          if ($init == 0) :
        ?>
            <div class="carousel-item active">
              <img src="./<?php echo $dato['big_img_rute_home']; ?>" class="d-block w-100" alt="...">
              <?php
              if ($dato['show_old'] == 1):
              ?>
              <div class="carousel-caption">
                <span class="btn btn-lg shadow btn-warning">Nuestros eventos</span>
              </div>
              <?php
              else :
              ?>
              <div class="carousel-caption">
                <a href="<?php echo $dato['payu']; ?>" class="btn btn-lg shadow btn-warning" target="_blank">Inscribirme</a>
              </div>
              <?php
                endif;
              ?>
            </div>
          <?php
          else :
          ?>
            <div class="carousel-item">
              <img src="./<?php echo $dato['big_img_rute_home']; ?>" class="d-block w-100" alt="...">
              <?php
              if ($dato['show_old'] == 1):
              ?>
              <div class="carousel-caption">
                <span class="btn btn-lg shadow btn-warning">Nuestros eventos</span>
              </div>
              <?php
              else :
              ?>
              <div class="carousel-caption">
                <a href="<?php echo $dato['payu']; ?>" class="btn btn-lg shadow btn-warning" target="_blank">Inscribirme</a>
              </div>
              <?php
                endif;
              ?>
            </div>
          <?php
            endif;
            $init++;
            endforeach;
            if ($init > 1) :
          ?>
          <button class="carousel-control-prev" type="button" data-bs-target="#sliderDeskop" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Anterior</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#sliderDeskop" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Siguiente</span>
          </button>
        <?php
        endif;
        ?>
      </div>
    </div>
    <div id="sliderMobile" class="carousel slide d-lg-none" data-bs-ride="carousel">
      <div class="carousel-inner">
        <?php
        $init = 0;
        foreach ($resultado as $dato) :
          if ($init == 0) :
        ?>
            <div class="carousel-item active">
              <img src="./<?php echo $dato['small_image_rute_home']; ?>" class="d-block w-100" alt="...">
              <?php
              if ($dato['show_old'] == 1):
              ?>
              <div class="carousel-caption">
                <span class="btn btn-lg shadow btn-warning">Nuestros eventos</span>
              </div>
              <?php
              else :
              ?>
              <div class="carousel-caption">
                <a href="<?php echo $dato['payu']; ?>" class="btn btn-lg shadow btn-warning" target="_blank">Inscribirme</a>
              </div>
              <?php
                endif;
              ?>
            </div>
          <?php
          else :
          ?>
            <div class="carousel-item">
              <img src="./<?php echo $dato['small_image_rute_home']; ?>" class="d-block w-100" alt="...">
              <?php
              if ($dato['show_old'] == 1):
              ?>
              <div class="carousel-caption">
                <span class="btn btn-lg shadow btn-warning">Nuestros eventos</span>
              </div>
              <?php
              else :
              ?>
              <div class="carousel-caption">
                <a href="<?php echo $dato['payu']; ?>" class="btn btn-lg shadow btn-warning" target="_blank">Inscribirme</a>
              </div>
              <?php
                endif;
              ?>
            </div>
          <?php
          endif;
          $init++;
        endforeach;
        if ($init > 1) :
          ?>
          <button class="carousel-control-prev" type="button" data-bs-target="#sliderMobile" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Anterior</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#sliderMobile" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Siguiente</span>
          </button>
        <?php
        endif;
        ?>
      </div>
    </div>
  </section>

  <section class="bg-secundary center-vertical">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12">
          <p class="h1 text-light">
            Estamos preparando para ti el mejor contenido de educación para el trabajo y desarrollo humano, dirigido al crecimiento organizacional y de sus profesionales.
          </p>
        </div>
      </div>
    </div>
  </section>

  <section class="block-bg-img image1"></section>

  <section class="bg-warning center-vertical py-5 py-md-0 socias">
    <div class="container">
      <div class="row align-content-center">
        <div class="col-12 col-md-6 ">
          <figure>
            <img src="/images/socias.jpg" alt="" class="img-fluid">
          </figure>
        </div>
        <div class="col-12 col-md-6 d-flex flex-column justify-content-center px-5">
          <div class="block-text">
            <h2>Nos dedicamos a la formación y consultoría para las organizaciones empresariales y las instituciones de educación superior.</h2>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="center-vertical servicios py-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-md-10 col-lg-4">
          <div class="card">
            <div class="card-body">
              <h3 class="card-title">Nuestros cursos</h3>
              <div class="card-text">Visualiza los factores de la vida que te están frenando y reconoce cómo tu enfoque del trabajo se traduce en resultados. Durante nuestras sesiones de formación, realizarás una auditoría completa de tu experiencia profesional e indentificarás oportunidades para un crecimiento saludable.</div>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-10 col-lg-4">
          <div class="card">
            <div class="card-body">
              <h3 class="card-title">Servicios de consultoría</h3>
              <div class="card-text">¿Necesitas una compresión más profunda de cómo convertir la superación personal en éxito empresarial? Permítenos ayudarte a llevar un cambio efectivo y sostenible al trabajo en tu organización. Juntos, podemos dar forma a un futuro exitoso para ti y tu empresa.</div>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-10 col-lg-4">
          <div class="card">
            <div class="card-body">
              <h3 class="card-title">Producción intelectual</h3>
              <div class="card-text">¿Requieres la elaboración de documentos que reflejen tu organización? Nosotros los realizamos por ti, ajustado a tus necesidades y requerimientos.</div>
            </div>
          </div>
        </div>
        <div class="col-12 text-center">
          <a href="servicios" class="btn btn-warning">Conoce nuestro portafolio completo</a>
        </div>
      </div>
    </div>
  </section>

  <section class="block-bg-img image2 center-vertical">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-md-6">
          <div class="card shadow-lg">
            <div class="card-body">
              <blockquote class="blockquote mb-0 text-center">
                <p>“Formal education will make you a living; self-education will make you a fortune.”</p>
                <p>“La educación formal te hará ganar la vida; la autoeducación te hará una fortuna ".</p>
                <span class="blockquote-footer text-warning"><cite title="Source Title">Jim Rohn</cite></span>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="clientes rmh">
    <div class="container text-center">
      <h2 class="my-5">Nuestros clientes</h2>
      <div class="sliderContainer">
        <ul class="list-inline">
          <li class="list-inline-item">
            <img src="./images/clientes/codema-logo.png" class="d-block w-100" alt="codema-logo">
            <div class="overlay">
              Charla dirigida a docentes para el manejo del estrés en la pandemia.
            </div>
          </li>
          <li class="list-inline-item">
            <img src="./images/clientes/jaia-logo.png" class="d-block w-100" alt="jaia-logo">
            <div class="overlay">
              Elaboración de taller para la expresión oral y corporal de sus colaboradores.
            </div>
          </li>
          <li class="list-inline-item">
            <img src="./images/clientes/pucp-logo.png" class="d-block w-100" alt="pucp-logo">
            <div class="overlay">
              Redacción de concepto sobre la convalidación de sus títulos ante el Ministerio de Educación Nacional.
            </div>
          </li>
        </ul>
      </div>
    </div>
  </section>

  <section class="center-vertical py-5 py-md-0">
    <div class="container">
      <div class="row align-content-center">
        <div class="col-12 col-md-6 d-flex flex-column justify-content-center px-5 pb-5 pb-md-0">
          <div class="block-text text-center">
            <h2>¿Tienes alguna pregunta?</h2>
            <p>Nos encantaría saber de ti. Ponte en contacto y comienza el viaje hacia el éxito.</p>
            <a href="contacto" class="btn btn-warning mt-2">Contáctanos</a>
          </div>
        </div>
        <div class="col-12 col-md-6 ">
          <figure>
            <img src="/images/socias-1.jpg" alt="" class="img-fluid">
          </figure>
        </div>
      </div>
    </div>
  </section>

  <?php
  include_once('./templates/footer.html');
  ?>