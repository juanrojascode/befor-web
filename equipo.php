<?php
include_once('./templates/header.html');
?>

<title>Nuestro equipo | Be for S.A.S.</title>
</head>

<body class="loadershow">

    <?php
    include_once('./templates/menu.html');
    ?>

    <section class="center-vertical titulo">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <p class="h1">
                        Nuestro equipo
                    </p>
                </div>
            </div>
        </div>
    </section>

    

    <?php
    include_once('./templates/footer.html');
    ?>