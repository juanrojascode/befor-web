<?php
session_start();

include_once 'conexion.php';

$usuario_login = $_POST['user'];
$contrasena_login = $_POST['pass'];


//VERIFICAR SI EL USUARIO EXISTE
$sql = 'SELECT * FROM users WHERE username = ?';
$sentencia = $pdo->prepare($sql);
$sentencia->execute(array($usuario_login));
$resultado = $sentencia->fetch();

if(!$resultado){
    //matar la operación
    echo 'No existe el usuario';
    die();
}

if( password_verify($contrasena_login, $resultado['password']) ){
    //las contraseñas son igual
    $_SESSION['admin'] = $usuario_login;
    header('location: ../panel-admin/index.php');
}else{
    echo 'No son iguales las contraseñas';
    die();
}