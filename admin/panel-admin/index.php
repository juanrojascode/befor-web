<?php
    if (!$_GET) {
        header('Location: index?pag=1');
    }
    
    session_start();

    if( isset($_SESSION['admin'])){

        include_once '../core/conexion.php';

        //LEER TABLA
        $sqlLeer = 'SELECT * FROM sliders';
        $gsent = $pdo->prepare($sqlLeer);
        $gsent->execute();
        $resultado = $gsent->fetchAll();

        $articleMaxView = 10;
        $numPageView = $gsent->rowCount();

        $pagView = $numPageView/$articleMaxView;
        $pagView = ceil($pagView);
        //echo ($pagView);

        $iniciar = ($_GET['pag']-1)*$articleMaxView;

        //Imprimir TABLA
        $sqlPrint = 'SELECT * FROM sliders ORDER BY date_mod DESC, hour_mod DESC LIMIT :iniciar,:narticle';
        $gsentPrint = $pdo->prepare($sqlPrint);
        $gsentPrint->bindParam(':iniciar', $iniciar, PDO::PARAM_INT);
        $gsentPrint->bindParam(':narticle', $articleMaxView, PDO::PARAM_INT);
        $gsentPrint->execute();
        $rPrint = $gsentPrint->fetchAll();

    }else{
        header('location:../index');
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php
    include 'head.html';
    ?>

    <title>Contenido || Panel Administrativo</title>

</head>
<body>
    <div class="wrapper">

        <?php
        include 'nav.html'
        ?>
        <section class="content">
            <div class="main-content container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-table publishContent card-border-color card-border-color-primary">
                            <!-- <div class="card-header">Contenido publicado</div> -->
                            <div class="card-body table-responsive">
                                <table class="table cPublicado table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Título</th>
                                            <th>Sección</th>
                                            <th>Creado</th>
                                            <th>Última edición</th>
                                            <th>Autor</th>
                                            <th>Operaciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($rPrint as $dato):
                                        ?>
                                        <tr>
                                            <td style="width:40%;">
                                                <?php echo $dato['title']; ?>
                                            </td>
                                            <td>
                                                Blog
                                            </td>
                                            <td>
                                                <?php echo $dato['date_create'].' - '.$dato['hour_create'] ?>
                                            </td>
                                            <td>
                                                <?php echo $dato['date_mod'].' - '.$dato['hour_mod'] ?>
                                            </td>
                                            <td>
                                                Autor
                                            </td>
                                            <td>
                                                <!-- Example split danger button -->
                                                <div class="btn-group">
                                                    <a class="btn btn-secondary" href="editar?id=<?php echo $dato['idSlider']; ?>">Editar</a>

                                                    <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false"><span class="visually-hidden">Toggle Dropdown</span></button>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a data-id="<?php echo $dato['idSlider']; ?>" class="dropdown-item" data-bs-toggle="modal" data-bs-target="#modalEliminar">Eliminar</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-center">
                                    <li class="page-item  <?php echo $_GET['pag'] <= 1 ? 'd-none' : '' ?>">
                                        <a class="page-link" href="index?pag=<?php echo $_GET['pag']-1 ?>">
                                        Anterior</a>
                                    </li>

                                    <?php for ($i=0; $i < $pagView; $i++): ?>
                                    <li class="page-item <?php echo $_GET['pag'] == ($i+1) ? 'active' : '' ?>">
                                        <a class="page-link" href="index?pag=<?php echo $i+1 ?>">
                                        <?php echo $i+1 ?>
                                        </a>
                                    </li>
                                    <?php endfor ?>

                                    <li class="page-item <?php echo $_GET['pag'] >= $pagView ? 'd-none' : '' ?>">
                                        <a class="page-link" href="index?pag=<?php echo $_GET['pag']+1 ?>">
                                        Siguiente</a>
                                    </li>
                                </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <div class="modal effect modal-warning" id="modalEliminar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <i class="fas fa-exclamation-triangle display-3"></i>
                        <h3 class="mb-4">¡Atención!</h3>
                        <p>Está tratando de borrar un artículo de la base de datos
                        <br>¿está seguro de hacerlo?</p>
                        <div class="mt-5">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancelar</button>
                            <a class="btn btn-secondary aceptar" href="">Aceptar</a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    
    <?php
    include 'scripts.html'
    ?>

</body>
</html>