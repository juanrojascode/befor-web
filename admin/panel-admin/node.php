<?php session_start();
    if( isset($_SESSION['admin'])){

        include_once '../core/conexion.php';

        $v = "<script>alert('Publicada')</script>";
        $f = "<script>alert('Todos los campos son requeridos.')</script>";

        //AGREGAR
        if($_POST){
            $articleTitle = $_POST['title'];
            /* $articleContent = $_POST['content_node']; */
            $payu = $_POST['payu'];
            $fechaCreada = $_POST['date_create'];
            $horaCreada = $_POST['hour_create'];
            $fechaMod = $_POST['date_mod'];
            $horaMod = $_POST['hour_mod'];
            $showSlider = $_POST['show_slider'];

            $showSliderValue = ($showSlider) ? 1 : 0 ;

            if($articleTitle == '' || $_FILES['bigimage']['name'] == null || $_FILES['smallimage']['name'] == null){

                echo $f;

            }else{
                    //Guardar imagen grande
                    $bigImage=$_FILES['bigimage']['name'];
                    $ruta=$_FILES['bigimage']['tmp_name'];
                    $bigDestino="../../images/eventos/".$bigImage;
                    $bigDestinoHome="images/eventos/".$bigImage ;
                    //Guardar imagen pequeña
                    $smallImage=$_FILES['smallimage']['name'];
                    $ruta2=$_FILES['smallimage']['tmp_name'];
                    $smallDestino="../../images/eventos/mobile/".$smallImage;
                    $smallDestinoHome="images/eventos/mobile/".$smallImage ;
                    
                    if(copy($ruta,$bigDestino) && copy($ruta2,$smallDestino)){
                        $sql_agregar1 = 'INSERT INTO sliders (title, big_img_rute, big_img_rute_home, small_image_rute, small_image_rute_home, payu, date_create, hour_create, date_mod, hour_mod, show_slider) VALUES (?,?,?,?,?,?,?,?,?,?,?)';
                        $sentencia_agregar1 = $pdo->prepare($sql_agregar1);
                        $sentencia_agregar1->execute(array($articleTitle,$bigDestino,$bigDestinoHome,$smallDestino,$smallDestinoHome,$payu,$fechaCreada,$horaCreada,$fechaMod,$horaMod,$showSliderValue));

                        echo $v;
                        header('location: index?pag=1');
                    }
            }

            
            $sentencia_agregar1 = null;
            //$sentencia_agregar = null;
            $pdo = null;
        }

        date_default_timezone_set('America/Bogota');

    }else{
        header('location:../index');
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php
    include 'head.html'
    ?>

    <title>Crear Artículo || Panel Administrativo</title>

    <!-- <style>
        .cke_contents{
            height: 355px !important;
        }
        hr{
            margin: 2rem 0rem
        }
    </style>

    <script src="../ckeditor/ckeditor.js"></script> -->
    
    

</head>
<body>
    <div class="wrapper">

        <?php
        include 'nav.html'
        ?>

        <section class="content ml-0">
            <div class="main-content container-fluid">
                <form method="POST" enctype="multipart/form-data" class="g-2">
                    <div class="row g-3 justify-content-center">
                        <div class="col-12 col-md-8 card card-border-color card-border-color-primary">
                            <div class="card-body">
                                <div class="mb-3">
                                    <label class="font-weight-bold form-label" for="title">Título <span class="text-danger">*</span></label>
                                    <input class="form-control" name="title">
                                </div>
                                <!-- <div class="mb-3">
                                    <label>Contenido</label>
                                    <textarea name="content_node" id="editor1"></textarea>
                                </div> -->
                                <div class="mb-3">
                                    <label class="font-weight-bold form-label" form="payu">URL PayU</label>
                                    <input class="form-control" name="payu">
                                </div>
                                <div class="mb-3">
                                    <label id="imgSelect" class="custom-file-label" for="bigimage">Grande (1200x330px)</label>
                                    <input type="file" name="bigimage" class="form-control">   
                                </div>   
                                <div class="mb-3">
                                    <label id="imgSelect2" class="custom-file-label" for="smallimage">Pequeña (1:1)</label>
                                    <input type="file" name="smallimage" class="form-control">
                                </div> 
                                <label class="mb-3">Fecha de publicación:</label>
                                <div class="row">
                                    <div class="col-12 col-md-7 mb-3">
                                        <input type="date" class="form-control" value="<?php echo date('Y-m-d') ?>" name="date_create" id="date">
                                    </div>
                                    <div class="col-12 col-md-5 mb-3">
                                        <input type="time" class="form-control" value="<?php echo date('h:i') ?>" name="hour_create" id="datetime">
                                    </div>
                                </div>
                                <hr>
                                <div class="d-none">
                                    <input type="date" class="form-control" value="<?php echo date('Y-m-d') ?>" name="date_mod" id="date">
                                    <input type="time" class="form-control" value="<?php echo date('h:i') ?>" name="hour_mod" id="datetime">
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" name="show_slider" id="show_slider">
                                    <label class="form-check-label" for="show_slider">
                                        Mostrar en slider
                                    </label>
                                </div>
                                <button class="btn btn-lg btn-primary mt-3 w-100" type="submit">Publicar</button> 
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        
    </div>

    
    <?php
    include 'scripts.html'
    ?>
    <!-- <script>
        CKEDITOR.replace( 'editor1' )
    </script> -->

</body>
</html>

