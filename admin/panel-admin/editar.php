<?php session_start();
    if( isset($_SESSION['admin'])){

        include_once '../core/conexion.php';

        $id = $_GET['id'];

        $sqlLeer = 'SELECT * FROM articles WHERE id=?';
        $gsent = $pdo->prepare($sqlLeer);
        $gsent->execute(array($id));
        $resultado = $gsent->fetchAll();

        if($_POST){
            $idEdit = $_POST['id'];
            $articleTitle = $_POST['title_node'];
            $articleContent = $_POST['content_node'];
            $articleTemplate = $_POST['select_template'];
            $fechaCreada = $_POST['fecha_creada'];
            $horaCreada = $_POST['hora_creada'];
            $fechaMod = $_POST['fecha_mod'];
            $horaMod = $_POST['hora_mod'];
            
            if( $_FILES['img']['name'] != null ){
                $articleImage=$_FILES['img']['name'];
                $ruta=$_FILES['img']['tmp_name'];
                $destino="../../images/articles/".$articleImage;
                $destinoHome="images/articles/".$articleImage ;
                
                if(copy($ruta,$destino)){
                    $sql_agregar = 'UPDATE articles SET title_node=?,content_node=?,select_template=?,ruta_imagen=?,ruta_imagen_home=?,fecha_creada=?,hora_creada=?,fecha_mod=?,hora_mod=? WHERE id=?';
                    $sentencia_agregar = $pdo->prepare($sql_agregar);
                    $sentencia_agregar->execute(array($articleTitle,$articleContent,$articleTemplate,$destino,$destinoHome,$fechaCreada,$horaCreada,$fechaMod,$horaMod,$idEdit));
                }
            }else{
                $sql_editar = 'UPDATE articles SET title_node=?,content_node=?,select_template=?,fecha_creada=?,hora_creada=?,fecha_mod=?,hora_mod=? WHERE id=?';
                $sentencia_editar = $pdo->prepare($sql_editar);
                $sentencia_editar->execute(array($articleTitle,$articleContent,$articleTemplate,$fechaCreada,$horaCreada,$fechaMod,$horaMod,$idEdit));
            }

            $sentencia_editar = null;
            $pdo = null;
            header('location:index?pag=1');
        }

        date_default_timezone_set('America/Bogota');

    }else{
        header('location:../index');
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php
    include 'head.html'
    ?>
    <title>Editar Artículo || Panel Administrativo</title>

    <style>
        .cke_contents{
            height: 355px !important;
        }
    </style>

    <script src="../ckeditor/ckeditor.js"></script>    

</head>
<body>
    <div class="wrapper">

        <?php
        include 'nav.html'
        ?>

        <section class="content ml-0">
            <div class="main-content container-fluid">
                <?php foreach ($resultado as $resultado_unico): ?>
                <form enctype="multipart/form-data" method="POST">
                    <div class="form-row justify-content-center">
                        <div class="card col-12 col-md-8 card-border-color card-border-color-primary">
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="font-weight-bold">Título <span class="text-danger">*</span></label>
                                    <input class="form-control" name="title_node" rows="1" value="<?php echo $resultado_unico['title_node']; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Contenido</label>
                                    <textarea name="content_node" id="editor1">
                                        <?php echo $resultado_unico['content_node']; ?>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card col-12 col-md-3 card-border-color card-border-color-primary">
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="mr-2 align-self-start">Tipo de Plantilla:</label>
                                    <div class="select-template mt-3 ml-2">
                                        <?php 
                                            if ( $resultado_unico['select_template'] == 6) {
                                                echo (' <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="6" checked>
                                                            <label class="form-check-label"><img src="images/layout1.svg" alt="Template grande" width="45px"></label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="4">
                                                            <label class="form-check-label"><img src="images/layout2.svg" alt="Template mediano" width="45px"></label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="3">
                                                            <label class="form-check-label"><img src="images/layout3.svg" alt="Template pequeño" width="45px"></label>
                                                        </div>');
                                            }if ( $resultado_unico['select_template'] == 4) {
                                                echo (' <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="6">
                                                            <label class="form-check-label"><img src="images/layout1.svg" alt="Template grande" width="45px"></label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="4" checked>
                                                            <label class="form-check-label"><img src="images/layout2.svg" alt="Template mediano" width="45px"></label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="3">
                                                            <label class="form-check-label"><img src="images/layout3.svg" alt="Template pequeño" width="45px"></label>
                                                        </div>');
                                            }if ( $resultado_unico['select_template'] == 3) {
                                                echo (' <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="6">
                                                            <label class="form-check-label"><img src="images/layout1.svg" alt="Template grande" width="45px"></label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="4">
                                                            <label class="form-check-label"><img src="images/layout2.svg" alt="Template mediano" width="45px"></label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="3" checked>
                                                            <label class="form-check-label"><img src="images/layout3.svg" alt="Template pequeño" width="45px"></label>
                                                        </div>');
                                            }
                                        ?>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="mb-0">Portada:</label>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="custom-file">
                                        <input type="file" onChange="viewimg()" name="img" class="custom-file-input">
                                        <label id="imgSelect" class="custom-file-label" for="inputGroupFile01">Cambiar imagen</label>
                                    </div>
                                </div>  
                                <div class="form-group">
                                    <label for="imagen">Imagen actual:</label><br>
                                    <img src="<?php echo $resultado_unico['ruta_imagen']; ?>" alt="" width="120px" height="auto">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="mb-0">Fecha de publicación</label>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-7">
                                    <input type="date" class="form-control" value="<?php echo $resultado_unico['fecha_creada'] ?>" name="fecha_creada" id="date">
                                    </div>
                                    <div class="form-group col-12 col-md-5">
                                        <input type="time" class="form-control" value="<?php echo $resultado_unico['hora_creada'] ?>" name="hora_creada" id="datetime">
                                    </div>
                                </div>
                                <hr>
                                <div class="d-none">
                                    <input type="date" class="form-control" value="<?php echo date('Y-m-d') ?>" name="fecha_mod" id="date">
                                    <input type="time" class="form-control" value="<?php echo date('h:i') ?>" name="hora_mod" id="datetime">
                                    <input type="hidden" value="<?php echo $resultado_unico['id']; ?>" name="id">
                                </div>
                                <button class="btn btn-lg btn-primary mt-3 w-100" type="submit">Guardar</button> 
                            </div>
                        </div>   
                    </div>
                </form>
                <?php endforeach ?>
            </div>
        </section>

    </div>

    
    <?php
    include 'scripts.html'
    ?>
    <script>
        CKEDITOR.replace( 'editor1' )
    </script>

</body>
</html>