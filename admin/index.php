<?php session_start();
if (isset($_SESSION['admin'])) {
    header('location:panel-admin/');
} else {
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Be for S.A.S.</title>

    <link rel="stylesheet" href="../css/custom.css">

    <style>
        body {
            height: 100vh;
            display: flex;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .form-signin {
            width: 100%;
            max-width: 330px;
            padding: 15px;
            margin: auto;
        }

        input:focus{
            background-color: rgba(255, 255, 255, 0.5) !important
        }
    </style>

</head>

<body>
    <div class="container d-flex justify-content-center align-items-center">
        <form action="core/login.php" method="POST" class="text-center">

            <img class="mb-4" src="../images/logo.webp" alt="Logo Be for" width="72">            
            <h1 class="h3 mb-3 fw-normal">Iniciar sesión</h1>

            <div class="form-floating mb-3">
                <input type="text" class="form-control" id="floatingInput" name="user" placeholder="name@example.com">
                <label for="floatingInput">Usuario</label>
            </div>
            <div class="form-floating mb-3">
                <input type="text" class="form-control" id="floatingPassword" name="pass" placeholder="Password">
                <label for="floatingPassword">Contraseña</label>
            </div>

            <button class="w-100 btn btn-lg btn-warning" type="submit">Entrar</button>

        </form>
    </div>
</body>
</html>